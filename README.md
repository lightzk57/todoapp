# TodoApp

Todo App

# How to using app

This app have 3 main screen: 
- All Todos: Include complete and incomplete Todos
- Complete Todos
- Incomplete Todos

Each todo item have a checkbox to update complete, incomplete status
- FloatingAction button to create new Todo: When create success, default incomplete status
- CreateTodoScreen: Provide id (must have), name (must have) and description
- Tap on a Todo to edit or remove it

# Test case
- Using bloc to control state of BottomNavigator, when tap on an NavItem, screen will tranfer to corresponding screen, state in bloc will store index
- When loading data state will emits 2 state: loading -> success/ failure
- When update data (delete, update status of todo item) state will emits 2 state: processing -> success/ updateFailure