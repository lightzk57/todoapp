import 'package:flutter/material.dart';
import 'package:todo_app/constant/app_constant.dart';
import 'package:todo_app/domain/todo/entities/todo_entity.dart';

class TodoItem extends StatelessWidget {
  const TodoItem({
    Key? key,
    required this.onUpdateStatus,
    required this.todoModel,
    required this.onTap,
  }) : super(key: key);

  final ValueChanged onUpdateStatus;
  final GestureTapCallback onTap;
  final TodoEntity todoModel;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Expanded(
            child: Container(
              color: Colors.transparent,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    todoModel.title,
                    style: const TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                  Text(todoModel.description),
                ],
              ),
            ),
          ),
          Checkbox(
            value: todoModel.isCompleted == AppConstants.completed,
            onChanged: onUpdateStatus,
          )
        ],
      ),
    );
  }
}
