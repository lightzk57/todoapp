import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';
import 'package:todo_app/app/app_state.dart';
import 'package:todo_app/constant/app_constant.dart';
import 'package:todo_app/constant/size_constant.dart';
import 'package:todo_app/domain/todo/entities/todo_entity.dart';
import 'package:todo_app/domain/todo/redux/middleware_actions.dart';
import 'package:todo_app/presentation/screen/todo_detail/todo_detail_screen.dart';
import 'package:todo_app/presentation/widgets/todos_vm.dart';
import 'package:todo_app/utils/utilities.dart';

import 'widgets/todo_item.dart';

class TodoScreen extends StatefulWidget {
  const TodoScreen({Key? key, required this.title, required this.type})
      : super(key: key);
  final String title;
  final int type;

  @override
  State<TodoScreen> createState() => _TodoScreenState();
}

class _TodoScreenState extends State<TodoScreen> {
  int get type => widget.type;
  String get title => widget.title;
  Store? _store;

  @override
  void didChangeDependencies() {
    _store ??= StoreProvider.of<AppState>(context);

    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text(title)),
      body: SafeArea(
        child: RefreshIndicator(
          onRefresh: () async {
            await Future.delayed(const Duration(seconds: 2));
            _store!.dispatch(
              GetTodosMiddlewareTodoAction.create(
                  statusKey: AppConstants.getKey, isRefresh: true),
            );
          },
          child: SizedBox(
            height: double.infinity,
            child: Column(
              children: [
                StoreConnector<AppState, TodosVM>(
                  distinct: true,
                  converter: (Store<AppState> store) {
                    return TodosVM.createVM(store, key: type);
                  },
                  builder: (BuildContext context, TodosVM todosVM) {
                    return Expanded(child: _buildTodoData(todosVM));
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildTodoData(TodosVM todosVM) {
    final statusName = todosVM.status.state.name;
    if (statusName == AppConstants.loading) {
      return const Center(child: CircularProgressIndicator());
    }

    if (statusName == AppConstants.error) {
      return Center(child: Text(todosVM.status.msg));
    }

    List<TodoEntity> todos =
        Utilities.getTodosByType(todosVM.todos.toList(), type);

    return ListView.separated(
      itemCount: todos.length,
      padding: const EdgeInsets.symmetric(
        horizontal: kDefaultPadding,
        vertical: kMediumPadding,
      ),
      separatorBuilder: (_, index) => const SizedBox(
        height: kDefaultPadding,
      ),
      itemBuilder: (_, index) {
        final todo = todos[index];
        return TodoItem(
          onTap: () => Navigator.push(
            context,
            MaterialPageRoute(
              builder: (_) => TodoDetailScreen(todoModel: todo),
            ),
          ),
          onUpdateStatus: (value) {
            // Change value before call event
            int isCompleted = todo.isCompleted == AppConstants.completed
                ? AppConstants.incomplete
                : AppConstants.completed;

            final newTodo = todo.rebuild(
              (updates) => updates..isCompleted = isCompleted,
            );

            _store!.dispatch(
              UpdateTodoMiddlewareTodoAction.create(
                todo: newTodo,
                statusKey: AppConstants.updateKey,
              ),
            );
          },
          todoModel: todo,
        );
      },
    );
  }
}
