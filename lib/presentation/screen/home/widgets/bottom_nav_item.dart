import 'package:flutter/material.dart';
import 'package:todo_app/constant/size_constant.dart';

class BottomNavItem extends StatelessWidget {
  const BottomNavItem({
    Key? key,
    required this.icon,
    required this.label,
    required this.isActive,
    required this.onTap,
  }) : super(key: key);

  final String icon;
  final String label;
  final bool isActive;
  final VoidCallback onTap;

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: InkWell(
        onTap: onTap,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset(
              icon,
              height: kIconSize,
              color: isActive ? Theme.of(context).primaryColor : null,
            ),
            const SizedBox(height: kMinimumPadding),
            Text(
              label,
              style: TextStyle(
                color: isActive ? Theme.of(context).primaryColor : null,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
