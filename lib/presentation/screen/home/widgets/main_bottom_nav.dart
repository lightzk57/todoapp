import 'package:flutter/material.dart';
import 'package:todo_app/domain/todo/entities/bottom_nav_model.dart';
import 'bottom_nav_item.dart';

class MainBottomNavigator extends StatelessWidget {
  const MainBottomNavigator({
    Key? key,
    required this.onChanged,
    required this.curentIndex,
  }) : super(key: key);

  final Function(int) onChanged;
  final int curentIndex;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: kBottomNavigationBarHeight + 4,
      child: Row(
        children: List.generate(navs.length, (index) {
          final nav = navs[index];
          return BottomNavItem(
            onTap: () => onChanged(index),
            icon: nav.icon,
            label: nav.label,
            isActive: index == curentIndex,
          );
        }),
      ),
    );
  }
}
