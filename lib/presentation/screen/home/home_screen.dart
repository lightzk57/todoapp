import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:todo_app/app/app_state.dart';
import 'package:todo_app/constant/app_constant.dart';
import 'package:todo_app/domain/todo/redux/middleware_actions.dart';
import 'package:todo_app/presentation/screen/todo_screen/todo_screen.dart';
import 'widgets/main_bottom_nav.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  late int _currentIndex;

  @override
  void initState() {
    _currentIndex = 0;
    super.initState();
  }

  @override
  void didChangeDependencies() {
    StoreProvider.of<AppState>(context).dispatch(
      GetTodosMiddlewareTodoAction.create(statusKey: AppConstants.getKey),
    );
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Center(
          child: IndexedStack(
            index: _currentIndex,
            children: const [
              TodoScreen(
                title: 'All Todos',
                type: AppConstants.getKey,
              ),
              TodoScreen(
                title: 'Complete Todos',
                type: AppConstants.todoComplete,
              ),
              TodoScreen(
                title: 'Incomplete Todos',
                type: AppConstants.todoIncomplete,
              ),
            ],
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () =>
            Navigator.of(context).pushNamed(AppConstants.routeCreateTask),
        child: const Icon(Icons.add),
      ),
      bottomNavigationBar: MainBottomNavigator(
        curentIndex: _currentIndex,
        onChanged: (index) => setState(() => _currentIndex = index),
      ),
    );
  }
}
