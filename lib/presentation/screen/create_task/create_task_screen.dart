import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';
import 'package:todo_app/app/app_state.dart';
import 'package:todo_app/constant/app_constant.dart';
import 'package:todo_app/constant/size_constant.dart';
import 'package:todo_app/domain/todo/entities/todo_entity.dart';
import 'package:todo_app/domain/todo/redux/middleware_actions.dart';
import 'package:todo_app/presentation/widgets/status_vm.dart';
import 'package:todo_app/presentation/widgets/todos_vm.dart';
import 'package:todo_app/utils/utilities.dart';

class CreateTaskScreen extends StatefulWidget {
  const CreateTaskScreen({Key? key}) : super(key: key);

  @override
  State<CreateTaskScreen> createState() => _CreateTaskScreenState();
}

class _CreateTaskScreenState extends State<CreateTaskScreen> {
  late TextEditingController _titleCtrl;
  late TextEditingController _descriptionCtrl;
  late TextEditingController _idCtrl;
  late Store _store;

  @override
  void initState() {
    _titleCtrl = TextEditingController();
    _descriptionCtrl = TextEditingController();
    _idCtrl = TextEditingController();
    super.initState();
  }

  @override
  void didChangeDependencies() {
    _store = StoreProvider.of<AppState>(context);
    super.didChangeDependencies();
  }

  @override
  void dispose() {
    _titleCtrl.dispose();
    _descriptionCtrl.dispose();
    _idCtrl.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => Utilities.hideKeyboard(context),
      child: Scaffold(
        appBar: AppBar(
          title: const Text('Create task'),
        ),
        body: SafeArea(
          child: SingleChildScrollView(
            padding: const EdgeInsets.symmetric(
              horizontal: kDefaultPadding,
              vertical: kMediumPadding,
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const Text('ID'),
                const SizedBox(height: kSmallPadding),
                TextFormField(
                  controller: _idCtrl,
                ),
                const SizedBox(height: kMediumPadding),
                const Text('Title'),
                const SizedBox(height: kSmallPadding),
                TextFormField(
                  controller: _titleCtrl,
                ),
                const SizedBox(height: kMediumPadding),
                const Text('Desciption'),
                const SizedBox(height: kSmallPadding),
                TextFormField(
                  controller: _descriptionCtrl,
                ),
                const SizedBox(height: 50),
                _buildListenCreateStatus(),
                _buildCreateTodoBtn(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  StoreConnector<AppState, TodosVM> _buildCreateTodoBtn() {
    return StoreConnector<AppState, TodosVM>(
      converter: (Store<AppState> store) {
        return TodosVM.createVM(store, key: AppConstants.createKey);
      },
      distinct: true,
      builder: (context, todosVM) {
        return Center(
          child: ElevatedButton(
            onPressed: _onCreateTodoSubmitted,
            child: const Text('CREATE'),
          ),
        );
      },
    );
  }

  Widget _buildListenCreateStatus() {
    return StoreConnector<AppState, StatusVM>(
      converter: (Store<AppState> store) {
        return StatusVM.createVM(store, key: AppConstants.createKey);
      },
      distinct: true,
      onWillChange: (_, statusVM) {
        final statusName = statusVM.status.state.name;
        if (statusName == AppConstants.processing) {
          Utilities.showLoading(context);
        }

        if (statusName == AppConstants.success) {
          Utilities.hideLoading(context);
          Navigator.pop(context, true);
          Utilities.showSnackBar(
            context,
            content: "Create todo successfully",
            snackBartype: SnackBartype.success,
          );
          _store.dispatch(
            GetTodosMiddlewareTodoAction.create(
                statusKey: AppConstants.getKey, isRefresh: true),
          );
        }

        if (statusName == AppConstants.error) {
          Utilities.hideLoading(context);
          Utilities.showSnackBar(
            context,
            content: statusVM.status.msg,
          );
        }
      },
      builder: (context, status) {
        return const SizedBox.shrink();
      },
    );
  }

  void _onCreateTodoSubmitted() {
    if (_idCtrl.text.isEmpty) {
      Utilities.showSnackBar(context, content: "Id can not be null or empty");
      return;
    }

    if (_titleCtrl.text.isEmpty) {
      Utilities.showSnackBar(context,
          content: "Title can not be null or empty");
      return;
    }

    final todo = TodoEntity(
      (updates) => updates
        ..id = _idCtrl.text
        ..title = _titleCtrl.text
        ..description = _descriptionCtrl.text.trim()
        ..isCompleted = AppConstants.incomplete,
    );

    // BlocProvider.of<TodoBloc>(context).add(CreateTodoEvent(todo));

    _store.dispatch(
      CreateTodoMiddlewareTodoAction.create(todo: todo),
    );
  }
}
