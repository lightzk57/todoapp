import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';
import 'package:todo_app/app/app_state.dart';
import 'package:todo_app/constant/app_constant.dart';
import 'package:todo_app/constant/size_constant.dart';
import 'package:todo_app/domain/todo/entities/todo_entity.dart';
import 'package:todo_app/domain/todo/redux/middleware_actions.dart';
import 'package:todo_app/presentation/widgets/listener_status.dart';
import 'package:todo_app/presentation/widgets/todos_vm.dart';
import 'package:todo_app/utils/utilities.dart';

class TodoDetailScreen extends StatefulWidget {
  const TodoDetailScreen({Key? key, required this.todoModel}) : super(key: key);

  final TodoEntity todoModel;

  @override
  State<TodoDetailScreen> createState() => _TodoDetailScreenState();
}

class _TodoDetailScreenState extends State<TodoDetailScreen> {
  TodoEntity get todoModel => widget.todoModel;

  late TextEditingController _titleCtrl;
  late TextEditingController _descriptionCtrl;

  @override
  void initState() {
    _titleCtrl = TextEditingController()..text = todoModel.title;
    _descriptionCtrl = TextEditingController()..text = todoModel.description;
    super.initState();
  }

  @override
  void dispose() {
    _titleCtrl.dispose();
    _descriptionCtrl.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => Utilities.hideKeyboard(context),
      child: Scaffold(
        appBar: AppBar(
          title: const Text('Todo detail'),
          actions: [
            IconButton(
              onPressed: _showDeleteConfirmDialog,
              icon: const Icon(Icons.delete),
            )
          ],
        ),
        body: SafeArea(
          child: SingleChildScrollView(
            padding: const EdgeInsets.symmetric(
              horizontal: kDefaultPadding,
              vertical: kMediumPadding,
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const StatusListnerWidget(
                  actionKey: AppConstants.updateKey,
                  successContent: "Update successfully",
                ),
                const StatusListnerWidget(
                  actionKey: AppConstants.deleteKey,
                  successContent: "Delete successfully",
                ),
                const Text('Title'),
                const SizedBox(height: kSmallPadding),
                TextFormField(controller: _titleCtrl),
                const SizedBox(height: kMediumPadding),
                const Text('Descibe'),
                const SizedBox(height: kSmallPadding),
                TextFormField(controller: _descriptionCtrl),
                const SizedBox(height: 50),
                _buildUpdateTodoBtn(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  StoreConnector<AppState, TodosVM> _buildUpdateTodoBtn() {
    return StoreConnector<AppState, TodosVM>(
      converter: (Store<AppState> store) {
        return TodosVM.createVM(store, key: AppConstants.updateKey);
      },
      distinct: true,
      builder: (context, todosVM) {
        return Center(
          child: ElevatedButton(
            onPressed: _onUpdateSubmitted,
            child: const Text('UPDATE'),
          ),
        );
      },
    );
  }

  void _showDeleteConfirmDialog() async {
    await Utilities.showConfirmDialog(
      context,
      title: 'DELETE',
      content: 'Are you sure you want to delete this task',
      onAcceptTapped: () {
        StoreProvider.of<AppState>(context).dispatch(
          DeleteTodoMiddlewareTodoAction.create(id: todoModel.id),
        );
      },
    );
  }

  void _onUpdateSubmitted() {
    if (_titleCtrl.text.isEmpty) {
      Utilities.showSnackBar(context,
          content: "Title can not be null or empty");
      return;
    }

    var newTodo = TodoEntity(
      (updates) => updates
        ..id = todoModel.id
        ..isCompleted = todoModel.isCompleted
        ..title = _titleCtrl.text.trim()
        ..description = _descriptionCtrl.text.trim(),
    );

    StoreProvider.of<AppState>(context).dispatch(
      UpdateTodoMiddlewareTodoAction.create(
        todo: newTodo,
        statusKey: AppConstants.updateKey,
      ),
    );
  }
}
