// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'status_vm.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$StatusVM extends StatusVM {
  @override
  final Status status;

  factory _$StatusVM([void Function(StatusVMBuilder)? updates]) =>
      (new StatusVMBuilder()..update(updates))._build();

  _$StatusVM._({required this.status}) : super._() {
    BuiltValueNullFieldError.checkNotNull(status, r'StatusVM', 'status');
  }

  @override
  StatusVM rebuild(void Function(StatusVMBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  StatusVMBuilder toBuilder() => new StatusVMBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is StatusVM && status == other.status;
  }

  @override
  int get hashCode {
    return $jf($jc(0, status.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'StatusVM')..add('status', status))
        .toString();
  }
}

class StatusVMBuilder implements Builder<StatusVM, StatusVMBuilder> {
  _$StatusVM? _$v;

  StatusBuilder? _status;
  StatusBuilder get status => _$this._status ??= new StatusBuilder();
  set status(StatusBuilder? status) => _$this._status = status;

  StatusVMBuilder();

  StatusVMBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _status = $v.status.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(StatusVM other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$StatusVM;
  }

  @override
  void update(void Function(StatusVMBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  StatusVM build() => _build();

  _$StatusVM _build() {
    _$StatusVM _$result;
    try {
      _$result = _$v ?? new _$StatusVM._(status: status.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'status';
        status.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'StatusVM', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,deprecated_member_use_from_same_package,lines_longer_than_80_chars,no_leading_underscores_for_local_identifiers,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new,unnecessary_lambdas
