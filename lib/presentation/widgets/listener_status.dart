
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';
import 'package:todo_app/app/app_state.dart';
import 'package:todo_app/constant/app_constant.dart';
import 'package:todo_app/presentation/widgets/status_vm.dart';
import 'package:todo_app/utils/utilities.dart';

class StatusListnerWidget extends StatelessWidget {
  const StatusListnerWidget(
      {Key? key, required this.actionKey, this.successContent})
      : super(key: key);

  final int actionKey;
  final String? successContent;

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, StatusVM>(
      converter: (Store<AppState> store) {
        return StatusVM.createVM(store, key: actionKey);
      },
      distinct: true,
      onWillChange: (_, statusVM) {
        final statusName = statusVM.status.state.name;
        if (statusName == AppConstants.processing) {
          Utilities.showLoading(context);
        }

        if (statusName == AppConstants.success) {
          Utilities.hideLoading(context);
          Navigator.pop(context);
          Utilities.showSnackBar(
            context,
            content: successContent ?? "Success",
            snackBartype: SnackBartype.success,
          );
        }

        if (statusName == AppConstants.error) {
          Utilities.hideLoading(context);
          Utilities.showSnackBar(
            context,
            content: statusVM.status.msg,
          );
        }
      },
      builder: (context, status) {
        return const SizedBox.shrink();
      },
    );
  }
}
