import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:redux/redux.dart';
import 'package:todo_app/app/app_state.dart';
import 'package:todo_app/app/status.dart';
import 'package:todo_app/domain/todo/entities/todo_entity.dart';

part 'todos_vm.g.dart';

abstract class TodosVM implements Built<TodosVM, TodosVMBuilder> {
  BuiltList<TodoEntity> get todos;
  Status get status;

  factory TodosVM.createVM(Store<AppState> store, {required int key}) {
    final todoState = store.state.todoState;
    return TodosVM(
      (updates) => updates
        ..status = (todoState.statuses[key] ?? Status.idle()).toBuilder()
        ..todos = ListBuilder<TodoEntity>(todoState.todos?.toList() ?? [])
    );
  }

  TodosVM._();
  factory TodosVM([void Function(TodosVMBuilder)? updates]) = _$TodosVM;
}
