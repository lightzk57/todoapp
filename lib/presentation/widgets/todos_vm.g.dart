// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'todos_vm.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$TodosVM extends TodosVM {
  @override
  final BuiltList<TodoEntity> todos;
  @override
  final Status status;

  factory _$TodosVM([void Function(TodosVMBuilder)? updates]) =>
      (new TodosVMBuilder()..update(updates))._build();

  _$TodosVM._({required this.todos, required this.status}) : super._() {
    BuiltValueNullFieldError.checkNotNull(todos, r'TodosVM', 'todos');
    BuiltValueNullFieldError.checkNotNull(status, r'TodosVM', 'status');
  }

  @override
  TodosVM rebuild(void Function(TodosVMBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  TodosVMBuilder toBuilder() => new TodosVMBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is TodosVM && todos == other.todos && status == other.status;
  }

  @override
  int get hashCode {
    return $jf($jc($jc(0, todos.hashCode), status.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'TodosVM')
          ..add('todos', todos)
          ..add('status', status))
        .toString();
  }
}

class TodosVMBuilder implements Builder<TodosVM, TodosVMBuilder> {
  _$TodosVM? _$v;

  ListBuilder<TodoEntity>? _todos;
  ListBuilder<TodoEntity> get todos =>
      _$this._todos ??= new ListBuilder<TodoEntity>();
  set todos(ListBuilder<TodoEntity>? todos) => _$this._todos = todos;

  StatusBuilder? _status;
  StatusBuilder get status => _$this._status ??= new StatusBuilder();
  set status(StatusBuilder? status) => _$this._status = status;

  TodosVMBuilder();

  TodosVMBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _todos = $v.todos.toBuilder();
      _status = $v.status.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(TodosVM other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$TodosVM;
  }

  @override
  void update(void Function(TodosVMBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  TodosVM build() => _build();

  _$TodosVM _build() {
    _$TodosVM _$result;
    try {
      _$result =
          _$v ?? new _$TodosVM._(todos: todos.build(), status: status.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'todos';
        todos.build();
        _$failedField = 'status';
        status.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'TodosVM', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,deprecated_member_use_from_same_package,lines_longer_than_80_chars,no_leading_underscores_for_local_identifiers,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new,unnecessary_lambdas
