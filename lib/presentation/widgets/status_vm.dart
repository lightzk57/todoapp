import 'package:built_value/built_value.dart';
import 'package:redux/redux.dart';
import 'package:todo_app/app/app_state.dart';
import 'package:todo_app/app/status.dart';

part 'status_vm.g.dart';

abstract class StatusVM implements Built<StatusVM, StatusVMBuilder> {
  Status get status;

  factory StatusVM.createVM(Store<AppState> store, {required int key}) {
    final todoState = store.state.todoState;
    return StatusVM(
      (updates) => updates
        ..status = todoState.statuses[key]?.toBuilder() ?? Status.idle().toBuilder()
    );
  }

  StatusVM._();
  factory StatusVM([void Function(StatusVMBuilder)? updates]) = _$StatusVM;
}
