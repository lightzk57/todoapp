import 'package:built_value/built_value.dart';

part 'todo_entity.g.dart';

abstract class TodoEntity implements Built<TodoEntity, TodoEntityBuilder> {
  String get id;
  String get title;
  String get description;
  int get isCompleted;

  factory TodoEntity.fromJson(Map<String, dynamic> json) {
    // id = json['Id'];
    // title = json['Title'];
    // description = json['Description'];
    // isCompleted = json['IsCompleted'];
    return TodoEntity(
      (updates) => updates
        ..id = json['Id']
        ..title = json['Title']
        ..description = json['Description']
        ..isCompleted = json['IsCompleted'],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['Id'] = id;
    data['Title'] = title;
    data['Description'] = description;
    data['IsCompleted'] = isCompleted;
    return data;
  }

  TodoEntity._();
  factory TodoEntity([void Function(TodoEntityBuilder)? updates]) =
      _$TodoEntity;

}
