import 'package:todo_app/constant/asset_constant.dart';

class BottomNavModel {
  final String label;
  final String icon;

  const BottomNavModel({required this.icon, required this.label});
}

const List<BottomNavModel> navs = [
  BottomNavModel(
    label: "All",
    icon: AssetConstant.iconAll,
  ),
  BottomNavModel(
    label: "Complete",
    icon: AssetConstant.iconComplete,
  ),
  BottomNavModel(
    label: "Incomplete",
    icon: AssetConstant.iconIncomplete,
  ),
];
