// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'todo_entity.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$TodoEntity extends TodoEntity {
  @override
  final String id;
  @override
  final String title;
  @override
  final String description;
  @override
  final int isCompleted;

  factory _$TodoEntity([void Function(TodoEntityBuilder)? updates]) =>
      (new TodoEntityBuilder()..update(updates))._build();

  _$TodoEntity._(
      {required this.id,
      required this.title,
      required this.description,
      required this.isCompleted})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(id, r'TodoEntity', 'id');
    BuiltValueNullFieldError.checkNotNull(title, r'TodoEntity', 'title');
    BuiltValueNullFieldError.checkNotNull(
        description, r'TodoEntity', 'description');
    BuiltValueNullFieldError.checkNotNull(
        isCompleted, r'TodoEntity', 'isCompleted');
  }

  @override
  TodoEntity rebuild(void Function(TodoEntityBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  TodoEntityBuilder toBuilder() => new TodoEntityBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is TodoEntity &&
        id == other.id &&
        title == other.title &&
        description == other.description &&
        isCompleted == other.isCompleted;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc($jc(0, id.hashCode), title.hashCode), description.hashCode),
        isCompleted.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'TodoEntity')
          ..add('id', id)
          ..add('title', title)
          ..add('description', description)
          ..add('isCompleted', isCompleted))
        .toString();
  }
}

class TodoEntityBuilder implements Builder<TodoEntity, TodoEntityBuilder> {
  _$TodoEntity? _$v;

  String? _id;
  String? get id => _$this._id;
  set id(String? id) => _$this._id = id;

  String? _title;
  String? get title => _$this._title;
  set title(String? title) => _$this._title = title;

  String? _description;
  String? get description => _$this._description;
  set description(String? description) => _$this._description = description;

  int? _isCompleted;
  int? get isCompleted => _$this._isCompleted;
  set isCompleted(int? isCompleted) => _$this._isCompleted = isCompleted;

  TodoEntityBuilder();

  TodoEntityBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _id = $v.id;
      _title = $v.title;
      _description = $v.description;
      _isCompleted = $v.isCompleted;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(TodoEntity other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$TodoEntity;
  }

  @override
  void update(void Function(TodoEntityBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  TodoEntity build() => _build();

  _$TodoEntity _build() {
    final _$result = _$v ??
        new _$TodoEntity._(
            id: BuiltValueNullFieldError.checkNotNull(id, r'TodoEntity', 'id'),
            title: BuiltValueNullFieldError.checkNotNull(
                title, r'TodoEntity', 'title'),
            description: BuiltValueNullFieldError.checkNotNull(
                description, r'TodoEntity', 'description'),
            isCompleted: BuiltValueNullFieldError.checkNotNull(
                isCompleted, r'TodoEntity', 'isCompleted'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,deprecated_member_use_from_same_package,lines_longer_than_80_chars,no_leading_underscores_for_local_identifiers,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new,unnecessary_lambdas
