// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'todo_state.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$TodoState extends TodoState {
  @override
  final BuiltList<TodoEntity>? todos;
  @override
  final BuiltMap<int, Status> statuses;
  @override
  final String msg;

  factory _$TodoState([void Function(TodoStateBuilder)? updates]) =>
      (new TodoStateBuilder()..update(updates))._build();

  _$TodoState._({this.todos, required this.statuses, required this.msg})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(statuses, r'TodoState', 'statuses');
    BuiltValueNullFieldError.checkNotNull(msg, r'TodoState', 'msg');
  }

  @override
  TodoState rebuild(void Function(TodoStateBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  TodoStateBuilder toBuilder() => new TodoStateBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is TodoState &&
        todos == other.todos &&
        statuses == other.statuses &&
        msg == other.msg;
  }

  @override
  int get hashCode {
    return $jf(
        $jc($jc($jc(0, todos.hashCode), statuses.hashCode), msg.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'TodoState')
          ..add('todos', todos)
          ..add('statuses', statuses)
          ..add('msg', msg))
        .toString();
  }
}

class TodoStateBuilder implements Builder<TodoState, TodoStateBuilder> {
  _$TodoState? _$v;

  ListBuilder<TodoEntity>? _todos;
  ListBuilder<TodoEntity> get todos =>
      _$this._todos ??= new ListBuilder<TodoEntity>();
  set todos(ListBuilder<TodoEntity>? todos) => _$this._todos = todos;

  MapBuilder<int, Status>? _statuses;
  MapBuilder<int, Status> get statuses =>
      _$this._statuses ??= new MapBuilder<int, Status>();
  set statuses(MapBuilder<int, Status>? statuses) =>
      _$this._statuses = statuses;

  String? _msg;
  String? get msg => _$this._msg;
  set msg(String? msg) => _$this._msg = msg;

  TodoStateBuilder() {
    TodoState._setDefaults(this);
  }

  TodoStateBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _todos = $v.todos?.toBuilder();
      _statuses = $v.statuses.toBuilder();
      _msg = $v.msg;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(TodoState other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$TodoState;
  }

  @override
  void update(void Function(TodoStateBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  TodoState build() => _build();

  _$TodoState _build() {
    _$TodoState _$result;
    try {
      _$result = _$v ??
          new _$TodoState._(
              todos: _todos?.build(),
              statuses: statuses.build(),
              msg: BuiltValueNullFieldError.checkNotNull(
                  msg, r'TodoState', 'msg'));
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'todos';
        _todos?.build();
        _$failedField = 'statuses';
        statuses.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'TodoState', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,deprecated_member_use_from_same_package,lines_longer_than_80_chars,no_leading_underscores_for_local_identifiers,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new,unnecessary_lambdas
