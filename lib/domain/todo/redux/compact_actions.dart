// ignore_for_file: unused_field

import 'package:redux_compact/redux_compact.dart';
import 'package:todo_app/app/app_state.dart';
import 'package:todo_app/domain/todo/repo/todo_abstract_repository.dart';

abstract class TodoCompactAction extends CompactAction<AppState> {
  static late AbstractTodoRepository repository;
}

// class GetTodosCompactAction extends TodoCompactAction {
//   final int statusKey;
//   final bool isRefresh;

//   GetTodosCompactAction({this.statusKey = -1, this.isRefresh = false});

//   @override
//   AppState reduce() {
//     if (request.loading) {
//       return state.rebuild(
//         (updates) => updates.todoState
//           ..statuses[statusKey] =
//               isRefresh ? Status.processing() : Status.loading(),
//       );
//     } else if (request.hasError) {
//       return state.rebuild((updates) =>
//           updates.todoState..statuses[statusKey] = Status.error(this));
//     } else {
//       return state.rebuild(
//         (updates) => updates.todoState
//           ..statuses[statusKey] = Status.success()
//           ..todos = ListBuilder(request.data),
//       );
//     }
//   }

  // @override
  // Future<List<TodoEntity>> makeRequest() async {
  //   return await TodoCompactAction._repository.getTodos();
  // }
// }

// class CreateTodoCompactAction extends TodoCompactAction {
//   final TodoEntity todo;
//   final int statusKey;

//   CreateTodoCompactAction(this.todo, {this.statusKey = AppConstants.createKey});

//   @override
//   AppState reduce() {
//     if (request.loading) {
//       return state.rebuild(
//         (updates) =>
//             updates.todoState..statuses[statusKey] = Status.processing(),
//       );
//     } else if (request.hasError) {
//       return state.rebuild((updates) =>
//           updates.todoState..statuses[statusKey] = Status.error(this));
//     } else {
//       return state.rebuild(
//         (updates) => updates.todoState..statuses[statusKey] = Status.success(),
//       );
//     }
//   }

//   @override
//   Future<TodoEntity> makeRequest() async {
//     return await TodoCompactAction._repository.insertTodo(todo);
//   }
// }

// class UpdateTodoCompactAction extends TodoCompactAction {
//   final TodoEntity todo;
//   final int statusKey;

//   UpdateTodoCompactAction(this.todo, {this.statusKey = AppConstants.updateKey});

//   @override
//   AppState reduce() {
//     if (request.loading) {
//       return state.rebuild(
//         (updates) =>
//             updates.todoState..statuses[statusKey] = Status.processing(),
//       );
//     } else if (request.hasError) {
//       return state.rebuild((updates) =>
//           updates.todoState..statuses[statusKey] = Status.error(this));
//     } else {
//       final updateTodo = todo;
//       List<TodoEntity> todos = store.state.todoState.todos?.toList() ?? [];

      // int index = todos.indexWhere((todo) => todo.id == updateTodo.id);
      // todos.removeAt(index);
      // todos.insert(index, updateTodo);
//       return state.rebuild(
//         (updates) => updates.todoState
//           ..statuses[statusKey] = Status.success()
//           ..todos = ListBuilder(todos),
//       );
//     }
//   }

//   @override
//   Future<void> makeRequest() async {
//     return await TodoCompactAction._repository.updateTodo(todo);
//   }
// }

// class DeleteTodoCompactAction extends TodoCompactAction {
//   final String id;
//   final int statusKey;

//   DeleteTodoCompactAction(this.id, {this.statusKey = AppConstants.deleteKey});

//   @override
//   AppState reduce() {
//     if (request.loading) {
//       return state.rebuild(
//         (updates) =>
//             updates.todoState..statuses[statusKey] = Status.processing(),
//       );
//     } else if (request.hasError) {
//       return state.rebuild((updates) =>
//           updates.todoState..statuses[statusKey] = Status.error(this));
//     } else {
//       List<TodoEntity> todos = (store.state.todoState.todos?.toList() ?? [])
//         ..removeWhere((element) => element.id == id);

//       return state.rebuild(
//         (updates) => updates.todoState
//           ..statuses[statusKey] = Status.success()
//           ..todos = ListBuilder(todos),
//       );
//     }
//   }

//   @override
//   Future<void> makeRequest() async {
//     return await TodoCompactAction._repository.deleteTodo(id);
//   }
// }
