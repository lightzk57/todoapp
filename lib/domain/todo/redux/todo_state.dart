import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:todo_app/app/status.dart';
import 'package:todo_app/domain/todo/entities/todo_entity.dart';

part 'todo_state.g.dart';

abstract class TodoState implements Built<TodoState, TodoStateBuilder> {
  BuiltList<TodoEntity>? get todos;
  BuiltMap<int, Status> get statuses;
  String get msg;

  @BuiltValueHook(initializeBuilder: true)
  static void _setDefaults(TodoStateBuilder builder) => builder
    ..todos = ListBuilder()
    ..msg = ""
    ..statuses = MapBuilder();

  TodoState._();
  factory TodoState([void Function(TodoStateBuilder)? updates]) = _$TodoState;
}
