// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'middleware_actions.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$GetTodosMiddlewareTodoAction extends GetTodosMiddlewareTodoAction {
  @override
  final int statusKey;
  @override
  final bool isRefresh;

  factory _$GetTodosMiddlewareTodoAction(
          [void Function(GetTodosMiddlewareTodoActionBuilder)? updates]) =>
      (new GetTodosMiddlewareTodoActionBuilder()..update(updates))._build();

  _$GetTodosMiddlewareTodoAction._(
      {required this.statusKey, required this.isRefresh})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        statusKey, r'GetTodosMiddlewareTodoAction', 'statusKey');
    BuiltValueNullFieldError.checkNotNull(
        isRefresh, r'GetTodosMiddlewareTodoAction', 'isRefresh');
  }

  @override
  GetTodosMiddlewareTodoAction rebuild(
          void Function(GetTodosMiddlewareTodoActionBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  GetTodosMiddlewareTodoActionBuilder toBuilder() =>
      new GetTodosMiddlewareTodoActionBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is GetTodosMiddlewareTodoAction &&
        statusKey == other.statusKey &&
        isRefresh == other.isRefresh;
  }

  @override
  int get hashCode {
    return $jf($jc($jc(0, statusKey.hashCode), isRefresh.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'GetTodosMiddlewareTodoAction')
          ..add('statusKey', statusKey)
          ..add('isRefresh', isRefresh))
        .toString();
  }
}

class GetTodosMiddlewareTodoActionBuilder
    implements
        Builder<GetTodosMiddlewareTodoAction,
            GetTodosMiddlewareTodoActionBuilder> {
  _$GetTodosMiddlewareTodoAction? _$v;

  int? _statusKey;
  int? get statusKey => _$this._statusKey;
  set statusKey(int? statusKey) => _$this._statusKey = statusKey;

  bool? _isRefresh;
  bool? get isRefresh => _$this._isRefresh;
  set isRefresh(bool? isRefresh) => _$this._isRefresh = isRefresh;

  GetTodosMiddlewareTodoActionBuilder();

  GetTodosMiddlewareTodoActionBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _statusKey = $v.statusKey;
      _isRefresh = $v.isRefresh;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(GetTodosMiddlewareTodoAction other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$GetTodosMiddlewareTodoAction;
  }

  @override
  void update(void Function(GetTodosMiddlewareTodoActionBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  GetTodosMiddlewareTodoAction build() => _build();

  _$GetTodosMiddlewareTodoAction _build() {
    final _$result = _$v ??
        new _$GetTodosMiddlewareTodoAction._(
            statusKey: BuiltValueNullFieldError.checkNotNull(
                statusKey, r'GetTodosMiddlewareTodoAction', 'statusKey'),
            isRefresh: BuiltValueNullFieldError.checkNotNull(
                isRefresh, r'GetTodosMiddlewareTodoAction', 'isRefresh'));
    replace(_$result);
    return _$result;
  }
}

class _$CreateTodoMiddlewareTodoAction extends CreateTodoMiddlewareTodoAction {
  @override
  final int statusKey;
  @override
  final TodoEntity todo;

  factory _$CreateTodoMiddlewareTodoAction(
          [void Function(CreateTodoMiddlewareTodoActionBuilder)? updates]) =>
      (new CreateTodoMiddlewareTodoActionBuilder()..update(updates))._build();

  _$CreateTodoMiddlewareTodoAction._(
      {required this.statusKey, required this.todo})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        statusKey, r'CreateTodoMiddlewareTodoAction', 'statusKey');
    BuiltValueNullFieldError.checkNotNull(
        todo, r'CreateTodoMiddlewareTodoAction', 'todo');
  }

  @override
  CreateTodoMiddlewareTodoAction rebuild(
          void Function(CreateTodoMiddlewareTodoActionBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  CreateTodoMiddlewareTodoActionBuilder toBuilder() =>
      new CreateTodoMiddlewareTodoActionBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is CreateTodoMiddlewareTodoAction &&
        statusKey == other.statusKey &&
        todo == other.todo;
  }

  @override
  int get hashCode {
    return $jf($jc($jc(0, statusKey.hashCode), todo.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'CreateTodoMiddlewareTodoAction')
          ..add('statusKey', statusKey)
          ..add('todo', todo))
        .toString();
  }
}

class CreateTodoMiddlewareTodoActionBuilder
    implements
        Builder<CreateTodoMiddlewareTodoAction,
            CreateTodoMiddlewareTodoActionBuilder> {
  _$CreateTodoMiddlewareTodoAction? _$v;

  int? _statusKey;
  int? get statusKey => _$this._statusKey;
  set statusKey(int? statusKey) => _$this._statusKey = statusKey;

  TodoEntityBuilder? _todo;
  TodoEntityBuilder get todo => _$this._todo ??= new TodoEntityBuilder();
  set todo(TodoEntityBuilder? todo) => _$this._todo = todo;

  CreateTodoMiddlewareTodoActionBuilder();

  CreateTodoMiddlewareTodoActionBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _statusKey = $v.statusKey;
      _todo = $v.todo.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(CreateTodoMiddlewareTodoAction other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$CreateTodoMiddlewareTodoAction;
  }

  @override
  void update(void Function(CreateTodoMiddlewareTodoActionBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  CreateTodoMiddlewareTodoAction build() => _build();

  _$CreateTodoMiddlewareTodoAction _build() {
    _$CreateTodoMiddlewareTodoAction _$result;
    try {
      _$result = _$v ??
          new _$CreateTodoMiddlewareTodoAction._(
              statusKey: BuiltValueNullFieldError.checkNotNull(
                  statusKey, r'CreateTodoMiddlewareTodoAction', 'statusKey'),
              todo: todo.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'todo';
        todo.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'CreateTodoMiddlewareTodoAction', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

class _$UpdateTodoMiddlewareTodoAction extends UpdateTodoMiddlewareTodoAction {
  @override
  final int statusKey;
  @override
  final TodoEntity todo;

  factory _$UpdateTodoMiddlewareTodoAction(
          [void Function(UpdateTodoMiddlewareTodoActionBuilder)? updates]) =>
      (new UpdateTodoMiddlewareTodoActionBuilder()..update(updates))._build();

  _$UpdateTodoMiddlewareTodoAction._(
      {required this.statusKey, required this.todo})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        statusKey, r'UpdateTodoMiddlewareTodoAction', 'statusKey');
    BuiltValueNullFieldError.checkNotNull(
        todo, r'UpdateTodoMiddlewareTodoAction', 'todo');
  }

  @override
  UpdateTodoMiddlewareTodoAction rebuild(
          void Function(UpdateTodoMiddlewareTodoActionBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  UpdateTodoMiddlewareTodoActionBuilder toBuilder() =>
      new UpdateTodoMiddlewareTodoActionBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is UpdateTodoMiddlewareTodoAction &&
        statusKey == other.statusKey &&
        todo == other.todo;
  }

  @override
  int get hashCode {
    return $jf($jc($jc(0, statusKey.hashCode), todo.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'UpdateTodoMiddlewareTodoAction')
          ..add('statusKey', statusKey)
          ..add('todo', todo))
        .toString();
  }
}

class UpdateTodoMiddlewareTodoActionBuilder
    implements
        Builder<UpdateTodoMiddlewareTodoAction,
            UpdateTodoMiddlewareTodoActionBuilder> {
  _$UpdateTodoMiddlewareTodoAction? _$v;

  int? _statusKey;
  int? get statusKey => _$this._statusKey;
  set statusKey(int? statusKey) => _$this._statusKey = statusKey;

  TodoEntityBuilder? _todo;
  TodoEntityBuilder get todo => _$this._todo ??= new TodoEntityBuilder();
  set todo(TodoEntityBuilder? todo) => _$this._todo = todo;

  UpdateTodoMiddlewareTodoActionBuilder();

  UpdateTodoMiddlewareTodoActionBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _statusKey = $v.statusKey;
      _todo = $v.todo.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(UpdateTodoMiddlewareTodoAction other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$UpdateTodoMiddlewareTodoAction;
  }

  @override
  void update(void Function(UpdateTodoMiddlewareTodoActionBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  UpdateTodoMiddlewareTodoAction build() => _build();

  _$UpdateTodoMiddlewareTodoAction _build() {
    _$UpdateTodoMiddlewareTodoAction _$result;
    try {
      _$result = _$v ??
          new _$UpdateTodoMiddlewareTodoAction._(
              statusKey: BuiltValueNullFieldError.checkNotNull(
                  statusKey, r'UpdateTodoMiddlewareTodoAction', 'statusKey'),
              todo: todo.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'todo';
        todo.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            r'UpdateTodoMiddlewareTodoAction', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

class _$DeleteTodoMiddlewareTodoAction extends DeleteTodoMiddlewareTodoAction {
  @override
  final String id;
  @override
  final int statusKey;

  factory _$DeleteTodoMiddlewareTodoAction(
          [void Function(DeleteTodoMiddlewareTodoActionBuilder)? updates]) =>
      (new DeleteTodoMiddlewareTodoActionBuilder()..update(updates))._build();

  _$DeleteTodoMiddlewareTodoAction._(
      {required this.id, required this.statusKey})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        id, r'DeleteTodoMiddlewareTodoAction', 'id');
    BuiltValueNullFieldError.checkNotNull(
        statusKey, r'DeleteTodoMiddlewareTodoAction', 'statusKey');
  }

  @override
  DeleteTodoMiddlewareTodoAction rebuild(
          void Function(DeleteTodoMiddlewareTodoActionBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  DeleteTodoMiddlewareTodoActionBuilder toBuilder() =>
      new DeleteTodoMiddlewareTodoActionBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is DeleteTodoMiddlewareTodoAction &&
        id == other.id &&
        statusKey == other.statusKey;
  }

  @override
  int get hashCode {
    return $jf($jc($jc(0, id.hashCode), statusKey.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'DeleteTodoMiddlewareTodoAction')
          ..add('id', id)
          ..add('statusKey', statusKey))
        .toString();
  }
}

class DeleteTodoMiddlewareTodoActionBuilder
    implements
        Builder<DeleteTodoMiddlewareTodoAction,
            DeleteTodoMiddlewareTodoActionBuilder> {
  _$DeleteTodoMiddlewareTodoAction? _$v;

  String? _id;
  String? get id => _$this._id;
  set id(String? id) => _$this._id = id;

  int? _statusKey;
  int? get statusKey => _$this._statusKey;
  set statusKey(int? statusKey) => _$this._statusKey = statusKey;

  DeleteTodoMiddlewareTodoActionBuilder();

  DeleteTodoMiddlewareTodoActionBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _id = $v.id;
      _statusKey = $v.statusKey;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(DeleteTodoMiddlewareTodoAction other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$DeleteTodoMiddlewareTodoAction;
  }

  @override
  void update(void Function(DeleteTodoMiddlewareTodoActionBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  DeleteTodoMiddlewareTodoAction build() => _build();

  _$DeleteTodoMiddlewareTodoAction _build() {
    final _$result = _$v ??
        new _$DeleteTodoMiddlewareTodoAction._(
            id: BuiltValueNullFieldError.checkNotNull(
                id, r'DeleteTodoMiddlewareTodoAction', 'id'),
            statusKey: BuiltValueNullFieldError.checkNotNull(
                statusKey, r'DeleteTodoMiddlewareTodoAction', 'statusKey'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,deprecated_member_use_from_same_package,lines_longer_than_80_chars,no_leading_underscores_for_local_identifiers,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new,unnecessary_lambdas
