import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:redux_compact/redux_compact.dart';
import 'package:todo_app/app/app_state.dart';
import 'package:todo_app/app/status.dart';
import 'package:todo_app/constant/app_constant.dart';
import 'package:todo_app/domain/todo/entities/todo_entity.dart';

import 'compact_actions.dart';

part 'middleware_actions.g.dart';

abstract class GetTodosMiddlewareTodoAction extends Object
    with
        CompactAction<AppState>
    implements
        Built<GetTodosMiddlewareTodoAction,
            GetTodosMiddlewareTodoActionBuilder> {
  GetTodosMiddlewareTodoAction._();

  int get statusKey;
  bool get isRefresh;

  factory GetTodosMiddlewareTodoAction.create({
    required int statusKey,
    isRefresh = false,
  }) =>
      GetTodosMiddlewareTodoAction(
        (updates) => updates
          ..statusKey = statusKey
          ..isRefresh = isRefresh,
      );

  @override
  AppState reduce() {
    if (request.loading) {
      return state.rebuild(
        (updates) => updates.todoState
          ..statuses[statusKey] =
              isRefresh ? Status.processing() : Status.loading(),
      );
    }
    if (request.success) {
      return state.rebuild(
        (updates) => updates.todoState
          ..statuses[statusKey] = Status.success()
          ..todos = ListBuilder(request.data),
      );
    }
    if (request.hasError) {
      return state.rebuild((updates) => updates.todoState
        ..statuses[statusKey] = Status.error(
          request.error.toString(),
        ));
    }
    return state;
  }

  @override
  Future<List<TodoEntity>> makeRequest(Dispatch<AppState> dispatch) async {
    return await TodoCompactAction.repository.getTodos();
  }

  factory GetTodosMiddlewareTodoAction(
          [void Function(GetTodosMiddlewareTodoActionBuilder)? updates]) =
      _$GetTodosMiddlewareTodoAction;
}

//* CREATE
abstract class CreateTodoMiddlewareTodoAction extends Object
    with
        CompactAction<AppState>
    implements
        Built<CreateTodoMiddlewareTodoAction,
            CreateTodoMiddlewareTodoActionBuilder> {
  CreateTodoMiddlewareTodoAction._();

  int get statusKey;
  TodoEntity get todo;

  factory CreateTodoMiddlewareTodoAction.create({
    int statusKey = AppConstants.createKey,
    required TodoEntity todo,
  }) =>
      CreateTodoMiddlewareTodoAction(
        (updates) => updates
          ..statusKey = statusKey
          ..todo = todo.toBuilder(),
      );

  @override
  AppState reduce() {
    if (request.loading) {
      return state.rebuild(
        (updates) =>
            updates.todoState..statuses[statusKey] = Status.processing(),
      );
    } else if (request.success) {
      return state.rebuild((updates) =>
          updates.todoState..statuses[statusKey] = Status.success());
    } else if (request.hasError) {
      return state.rebuild((updates) => updates.todoState
        ..statuses[statusKey] = Status.error(
          request.error.toString(),
        ));
    }
    return state;
  }

  @override
  Future<TodoEntity> makeRequest(Dispatch<AppState> dispatch) async {
    return await TodoCompactAction.repository.insertTodo(todo);
  }

  factory CreateTodoMiddlewareTodoAction(
          [void Function(CreateTodoMiddlewareTodoActionBuilder)? updates]) =
      _$CreateTodoMiddlewareTodoAction;
}

//* UPDATE
abstract class UpdateTodoMiddlewareTodoAction extends Object
    with
        CompactAction<AppState>
    implements
        Built<UpdateTodoMiddlewareTodoAction,
            UpdateTodoMiddlewareTodoActionBuilder> {
  UpdateTodoMiddlewareTodoAction._();

  int get statusKey;
  TodoEntity get todo;

  factory UpdateTodoMiddlewareTodoAction.create({
    required int statusKey,
    required TodoEntity todo,
  }) =>
      UpdateTodoMiddlewareTodoAction(
        (updates) => updates
          ..statusKey = statusKey
          ..todo = todo.toBuilder(),
      );

  @override
  AppState reduce() {
    if (request.loading) {
      return state.rebuild(
        (updates) =>
            updates.todoState..statuses[statusKey] = Status.processing(),
      );
    }
    if (request.success) {
      final todos = state.todoState.todos?.toList() ?? [];
      final index = todos.indexWhere((element) => element.id == todo.id);
      todos.removeAt(index);
      todos.insert(index, todo);
      return state.rebuild(
        (updates) => updates.todoState
          ..statuses[statusKey] = Status.success()
          ..todos = ListBuilder(todos),
      );
    }
    if (request.hasError) {
      return state.rebuild((updates) => updates.todoState
        ..statuses[statusKey] = Status.error(
          request.error.toString(),
        ));
    }
    return state;
  }

  @override
  Future<int> makeRequest(Dispatch<AppState> dispatch) async {
    return await TodoCompactAction.repository.updateTodo(todo);
  }

  factory UpdateTodoMiddlewareTodoAction(
          [void Function(UpdateTodoMiddlewareTodoActionBuilder)? updates]) =
      _$UpdateTodoMiddlewareTodoAction;
}

//* DELETE
abstract class DeleteTodoMiddlewareTodoAction extends Object
    with
        CompactAction<AppState>
    implements
        Built<DeleteTodoMiddlewareTodoAction,
            DeleteTodoMiddlewareTodoActionBuilder> {
  DeleteTodoMiddlewareTodoAction._();
  String get id;
  int get statusKey;

  factory DeleteTodoMiddlewareTodoAction.create(
          {required String id, int statusKey = AppConstants.deleteKey}) =>
      DeleteTodoMiddlewareTodoAction(
        (updates) => updates
          ..id = id
          ..statusKey = statusKey,
      );

  @override
  AppState reduce() {
    if (request.loading) {
      return state.rebuild(
        (updates) =>
            updates.todoState..statuses[statusKey] = Status.processing(),
      );
    }
    if (request.success) {
      final newTodos = (state.todoState.todos?.toList() ?? [])
        ..removeWhere((element) => element.id == id);
      return state.rebuild(
        (updates) => updates.todoState
          ..statuses[statusKey] = Status.success()
          ..todos = ListBuilder(newTodos),
      );
    }
    if (request.hasError) {
      return state.rebuild((updates) => updates.todoState
        ..statuses[statusKey] = Status.error(
          request.error.toString(),
        ));
    }
    return state;
  }

  @override
  Future<int> makeRequest(Dispatch<AppState> dispatch) async {
    return await TodoCompactAction.repository.deleteTodo(id);
  }

  factory DeleteTodoMiddlewareTodoAction(
          [void Function(DeleteTodoMiddlewareTodoActionBuilder) updates]) =
      _$DeleteTodoMiddlewareTodoAction;
}
