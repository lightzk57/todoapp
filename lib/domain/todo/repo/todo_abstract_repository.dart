import 'package:todo_app/domain/todo/entities/todo_entity.dart';

abstract class AbstractTodoRepository {
  Future<int> deleteTodo(String id);

  Future<TodoEntity?> getOneItem(String id);

  Future<List<TodoEntity>> getTodos();

  Future<TodoEntity> insertTodo(TodoEntity todo);

  Future<int> updateTodo(TodoEntity todo);
}
