import 'package:sqflite/sqflite.dart';
import 'package:todo_app/constant/app_constant.dart';
import 'package:todo_app/domain/todo/entities/todo_entity.dart';
// ignore: depend_on_referenced_packages
import 'package:todo_app/domain/todo/repo/todo_abstract_repository.dart';

class TodoSqliteService implements AbstractTodoRepository {
  TodoSqliteService(this._database);

  final Database _database;

  @override
  Future<int> deleteTodo(String id) async {
    final result = await _database.delete(
      AppConstants.tblTodo,
      where: '${AppConstants.id} = ?',
      whereArgs: [id],
    );
    if (result == 0) throw Exception(AppConstants.errNoRowAffected);
    return result;
  }

  @override
  Future<TodoEntity?> getOneItem(String id) async {
    List<Map<String, dynamic>> maps = await _database.query(
      AppConstants.tblTodo,
      where: '${AppConstants.id}= ?',
      whereArgs: [id],
    );
    if (maps.isNotEmpty) return TodoEntity.fromJson(maps.first);
    return null;
  }

  @override
  Future<List<TodoEntity>> getTodos() async {
    List<Map<String, dynamic>> result =
        await _database.query(AppConstants.tblTodo);
    return result.map((e) => TodoEntity.fromJson(e)).toList();
  }

  @override
  Future<TodoEntity> insertTodo(TodoEntity todo) async {
    final existTodo = await getOneItem(todo.id);
    if (existTodo != null) {
      throw Exception(AppConstants.errIdAlredyExist);
    }
    final result = await _database.insert(AppConstants.tblTodo, todo.toJson());
    if (result == 0) throw Exception(AppConstants.errSaveTodoFailure);

    return TodoEntity(
      (updates) => updates
        ..id = result.toString()
        ..description = todo.description
        ..title = todo.title
        ..isCompleted = todo.isCompleted,
    );
  }

  @override
  Future<int> updateTodo(TodoEntity todo) async {
    return await _database.update(AppConstants.tblTodo, todo.toJson(),
        where: '${AppConstants.id} = ?', whereArgs: [todo.id]);
  }
}
