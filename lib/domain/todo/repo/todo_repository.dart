import 'package:todo_app/domain/todo/entities/todo_entity.dart';
import 'package:todo_app/domain/todo/repo/todo_abstract_repository.dart';

class TodoRepository implements AbstractTodoRepository {
  final AbstractTodoRepository remote;

  TodoRepository(this.remote);

  @override
  Future<int> deleteTodo(String id) async {
    return remote.deleteTodo(id);
  }

  @override
  Future<TodoEntity?> getOneItem(String id) {
    return remote.getOneItem(id);
  }

  @override
  Future<List<TodoEntity>> getTodos() {
    return remote.getTodos();
  }

  @override
  Future<TodoEntity> insertTodo(TodoEntity todo) {
    return remote.insertTodo(todo);
  }

  @override
  Future<int> updateTodo(TodoEntity todo) {
    return remote.updateTodo(todo);
  }
}
