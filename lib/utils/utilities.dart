// ignore_for_file: depend_on_referenced_packages

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

import 'package:todo_app/constant/app_constant.dart';
import 'package:todo_app/constant/size_constant.dart';
import 'package:todo_app/domain/todo/entities/todo_entity.dart';

enum SnackBartype { success, failure }

class Utilities {
  static final instance = Utilities._();
  Utilities._();

  static void hideKeyboard(BuildContext context) {
    FocusScope.of(context).requestFocus(FocusNode());
  }

  static void showLoading(BuildContext context) {
    showDialog(
      context: context,
      builder: (_) {
        return Scaffold(
          backgroundColor: Colors.transparent,
          body: Center(
            child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: Colors.white,
              ),
              alignment: Alignment.center,
              height: 70,
              width: 70,
              child: const CupertinoActivityIndicator(),
            ),
          ),
        );
      },
    );
  }

  static void hideLoading(BuildContext context) {
    Navigator.pop(context);
  }

  static Future showConfirmDialog(BuildContext context,
      {required String title,
      required String content,
      required Function onAcceptTapped}) async {
    await showDialog(
      context: context,
      builder: (_) {
        return Material(
          type: MaterialType.transparency,
          child: Center(
            child: Container(
              padding: const EdgeInsets.all(kMediumPadding),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(kDefaultRadius),
              ),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Text(title),
                  const SizedBox(height: kDefaultPadding),
                  Text(content),
                  const SizedBox(height: kMediumPadding),
                  Row(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      ElevatedButton(
                        onPressed: () => Navigator.pop(context),
                        child: const Text('NO'),
                      ),
                      const SizedBox(width: kMediumPadding),
                      ElevatedButton(
                        onPressed: () {
                          Navigator.pop(context);
                          onAcceptTapped();
                        },
                        style:
                            ElevatedButton.styleFrom(primary: Colors.red[400]),
                        child: const Text('YES'),
                      )
                    ],
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }

  static List<TodoEntity> getTodosByType(List<TodoEntity> todos, int type) {
    final newTodos = todos.reversed.toList();

    if (type == AppConstants.getKey) return newTodos;

    if (type == AppConstants.todoComplete) {
      return [
        ...newTodos
            .where((todo) => todo.isCompleted == AppConstants.completed)
            .toList(),
      ];
    }

    if (type == AppConstants.todoIncomplete) {
      return [
        ...newTodos
            .where((todo) => todo.isCompleted == AppConstants.incomplete)
            .toList(),
      ];
    }
    return [];
  }

  static void showSnackBar(
    BuildContext context, {
    required String content,
    SnackBartype snackBartype = SnackBartype.failure,
  }) {
    final snackBar = SnackBar(
      backgroundColor:
          snackBartype == SnackBartype.success ? Colors.green : Colors.red,
      duration: const Duration(seconds: AppConstants.snackBarDuration),
      content: Text(content),
    );

    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }

  static Future<Database> initDatabase() async {
    var dbPath = await getDatabasesPath();
    String path = join(dbPath, AppConstants.databaseName);

    final database = await openDatabase(
      path,
      onConfigure: (db) async {
        try {
          await db.execute(AppConstants.sqlCreateTableQuery);
        } catch (_) {}
      },
      version: 1,
      onCreate: (Database db, int version) async {
        await db.execute(AppConstants.sqlCreateTableQuery);
      },
    );
    return database;
  }
}
