extension StringX on String? {
  bool isNullOrEmpty() {
    return this == null || this!.isEmpty;
  }
}

class Validator {
  static final instance = Validator._();
  Validator._();
}
