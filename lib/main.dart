import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';
import 'package:redux_compact/redux_compact.dart';
import 'package:todo_app/app/app_middleware.dart';
import 'package:todo_app/app/app_reducer.dart';
import 'package:todo_app/app/app_state.dart';

import 'package:todo_app/config/routes.dart';
import 'package:todo_app/constant/size_constant.dart';
import 'package:todo_app/di/app_injector.dart';
import 'package:todo_app/domain/todo/redux/compact_actions.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  await configureDependencies();
  // await Utilities.initDatabase();
  final injector = AppInjector();
  final compactMiddleware = ReduxCompact.createMiddleware<AppState>();

  TodoCompactAction.repository  = await injector.abstractTodoRepository;

  runApp(StoreProvider(
    store: Store<AppState>(
      appReducer,
      initialState: AppState(),
      middleware: [
        ...appMiddleware(injector),
        compactMiddleware,
      ],
    ),
    child: const MyApp(),
  ));
  // runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        appBarTheme: const AppBarTheme(
          centerTitle: true,
        ),
        inputDecorationTheme: InputDecorationTheme(
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(kDefaultRadius),
          ),
        ),
      ),
      debugShowCheckedModeBanner: false,
      initialRoute: '/',
      routes: routes,
    );
  }
}
