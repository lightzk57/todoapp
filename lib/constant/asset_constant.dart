class AssetConstant {
  static final instance = AssetConstant._();
  AssetConstant._();

  static const String iconAll = 'assets/icons/all.png';
  static const String iconIncomplete = 'assets/icons/cancel.png';
  static const String iconComplete = 'assets/icons/checked.png';
}
