const double kIconSize = 20;
const double kDefaultRadius = 8;

const double kMinimumPadding = 5;
const double kSmallPadding = 8;
const double kDefaultPadding = 16;
const double kMediumPadding = 24;
