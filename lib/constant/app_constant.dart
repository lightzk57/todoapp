class AppConstants {
  static final instance = AppConstants._();
  AppConstants._();

  static const int snackBarDuration = 2;

  static const String errIdAlredyExist =
      "A Todo with the same id alredy exists, please try again";
  static const String errSaveTodoFailure =
      "Save todo failure, please try again";
  static const String errCantFindTodo = "Can not find todo with this id";
  static const String errNoRowAffected = "No row affected";
  static const String sqlCreateTableQuery =
      '''create table if not exists ${AppConstants.tblTodo} (
          ${AppConstants.id} text primary key, 
          ${AppConstants.title} text, 
          ${AppConstants.description} text,
          ${AppConstants.isCompleted} interger);''';

  //* ROUTES
  static const String routeCreateTask = '/createTask';

  //* TYPE
  static const int getKey = 0;
  static const int todoComplete = 1;
  static const int todoIncomplete = 2;
  static const int createKey = 3;
  static const int updateKey = 4;
  static const int deleteKey = 5;

  // IsCompleted
  static const int incomplete = 0;
  static const int completed = 1;

  // Database
  static const String databaseName = "Todo";
  static const String tblTodo = "tblTodo";
  static const String tblTodoRedux = "tblTodoRedux";
  static const String id = "Id";
  static const String title = "Title";
  static const String description = "Description";
  static const String isCompleted = "IsCompleted";

  // Status
  static const idle = 'idle';
  static const loading = 'loading';
  static const processing = 'processing';
  static const success = 'success';
  static const error = 'error';

  // Dependency injection
  static const dITodoMiddleware = 'TodoMiddleware';
  static const dIAstractTodoRepository = 'AbstractTodoRepository';
}
