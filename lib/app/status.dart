import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';

part 'status.g.dart';

abstract class Status implements Built<Status, StatusBuilder> {
  StatusState get state;
  String get msg;

  factory Status.idle() =>
      Status((updates) => updates..state = StatusState.idle);

  factory Status.loading() =>
      Status((updates) => updates..state = StatusState.loading);
      
  factory Status.processing() =>
      Status((updates) => updates..state = StatusState.processing);

  factory Status.success() =>
      Status((updates) => updates..state = StatusState.success);

  factory Status.error(Object error) => Status(
        (updates) => updates
          ..state = StatusState.error
          ..msg = error.toString(),
      );

  Status._();
  factory Status([void Function(StatusBuilder) updates]) = _$Status;

  @BuiltValueHook(initializeBuilder: true)
  static void _setDefaults(StatusBuilder builder) => builder.msg = '';
}

class StatusState extends EnumClass {
  static const StatusState idle = _$idle;
  static const StatusState loading = _$loading;
  static const StatusState processing = _$processing;
  static const StatusState success = _$success;
  static const StatusState error = _$error;

  const StatusState._(String name) : super(name);

  static BuiltSet<StatusState> get values => _$StatusStateValues;
  static StatusState valueOf(String name) => _$StatusStateValueOf(name);
}
