// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'status.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

const StatusState _$idle = const StatusState._('idle');
const StatusState _$loading = const StatusState._('loading');
const StatusState _$processing = const StatusState._('processing');
const StatusState _$success = const StatusState._('success');
const StatusState _$error = const StatusState._('error');

StatusState _$StatusStateValueOf(String name) {
  switch (name) {
    case 'idle':
      return _$idle;
    case 'loading':
      return _$loading;
    case 'processing':
      return _$processing;
    case 'success':
      return _$success;
    case 'error':
      return _$error;
    default:
      throw new ArgumentError(name);
  }
}

final BuiltSet<StatusState> _$StatusStateValues =
    new BuiltSet<StatusState>(const <StatusState>[
  _$idle,
  _$loading,
  _$processing,
  _$success,
  _$error,
]);

class _$Status extends Status {
  @override
  final StatusState state;
  @override
  final String msg;

  factory _$Status([void Function(StatusBuilder)? updates]) =>
      (new StatusBuilder()..update(updates))._build();

  _$Status._({required this.state, required this.msg}) : super._() {
    BuiltValueNullFieldError.checkNotNull(state, r'Status', 'state');
    BuiltValueNullFieldError.checkNotNull(msg, r'Status', 'msg');
  }

  @override
  Status rebuild(void Function(StatusBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  StatusBuilder toBuilder() => new StatusBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is Status && state == other.state && msg == other.msg;
  }

  @override
  int get hashCode {
    return $jf($jc($jc(0, state.hashCode), msg.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'Status')
          ..add('state', state)
          ..add('msg', msg))
        .toString();
  }
}

class StatusBuilder implements Builder<Status, StatusBuilder> {
  _$Status? _$v;

  StatusState? _state;
  StatusState? get state => _$this._state;
  set state(StatusState? state) => _$this._state = state;

  String? _msg;
  String? get msg => _$this._msg;
  set msg(String? msg) => _$this._msg = msg;

  StatusBuilder() {
    Status._setDefaults(this);
  }

  StatusBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _state = $v.state;
      _msg = $v.msg;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(Status other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$Status;
  }

  @override
  void update(void Function(StatusBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  Status build() => _build();

  _$Status _build() {
    final _$result = _$v ??
        new _$Status._(
            state: BuiltValueNullFieldError.checkNotNull(
                state, r'Status', 'state'),
            msg: BuiltValueNullFieldError.checkNotNull(msg, r'Status', 'msg'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,deprecated_member_use_from_same_package,lines_longer_than_80_chars,no_leading_underscores_for_local_identifiers,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new,unnecessary_lambdas
