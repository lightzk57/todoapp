import 'package:redux/redux.dart';
import 'package:redux_compact/redux_compact.dart';

import 'app_state.dart';

final appReducer = combineReducers<AppState>([
  // TypedReducer<AppState, dynamic>(processAppReducers),
  ReduxCompact.createReducer<AppState>(),
]);

// AppState processAppReducers(AppState state, dynamic action) {
//   return state.rebuild(
//     (builder) =>
//         builder..todoState = todoReducer(state.todoState, action).toBuilder(),
//   );
// }
