import 'package:built_value/built_value.dart';
import 'package:todo_app/domain/todo/redux/todo_domain.dart';
import 'package:todo_app/domain/todo/redux/todo_state.dart';

part 'app_state.g.dart';

abstract class AppState implements Built<AppState, AppStateBuilder>, TodoDomain {
  @override
  TodoState get todoState;

  AppState._();
  factory AppState([void Function(AppStateBuilder) updates]) = _$AppState;
}
