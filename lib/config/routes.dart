import 'package:flutter/material.dart';
import 'package:todo_app/constant/app_constant.dart';
import 'package:todo_app/presentation/screen/create_task/create_task_screen.dart';
import 'package:todo_app/presentation/screen/home/home_screen.dart';

final Map<String, WidgetBuilder> routes = {
  '/': (_) => const HomeScreen(),
  AppConstants.routeCreateTask: (_) => const CreateTaskScreen(),
};
