import 'package:todo_app/domain/todo/repo/todo_abstract_repository.dart';

// import '../domain/todo/redux/todo_middleware.dart';

abstract class AbstractSharedAppInjector {
  // TodoMiddleware get todoMiddleware;

  AbstractTodoRepository get abstractTodoRepository;
}
