import 'package:injectable/injectable.dart';
import 'package:todo_app/di/modules/abstract_todo_module.dart';

@Injectable(as: AbstractTodoModule)
class TodoModule extends AbstractTodoModule {}
