import 'package:injectable/injectable.dart';
import 'package:sqflite/sqflite.dart';
import 'package:todo_app/constant/app_constant.dart';
import 'package:todo_app/domain/todo/repo/todo_abstract_repository.dart';
import 'package:todo_app/domain/todo/repo/todo_repository.dart';
import 'package:todo_app/domain/todo/repo/todo_sqlite_service.dart';

@module
abstract class AbstractTodoModule {
  @singleton
  @Named(AppConstants.dIAstractTodoRepository)
  AbstractTodoRepository todoRepository(Database db) {
    return TodoRepository(TodoSqliteService(db));
  }

  // @singleton
  // @Named(AppConstants.dITodoMiddleware)
  // TodoMiddleware todoMiddleware(
  //     @Named(AppConstants.dIAstractTodoRepository)
  //         AbstractTodoRepository todoRepository) {
  //   return TodoMiddleware(todoRepository);
  // }
}
