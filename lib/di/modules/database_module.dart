import 'package:injectable/injectable.dart';
import 'package:sqflite/sqflite.dart';
import 'package:todo_app/utils/utilities.dart';

@module
abstract class DatabaseModule {
  @preResolve
  Future<Database> get database => Utilities.initDatabase();
}
