import 'package:get_it/get_it.dart';
import 'package:injectable/injectable.dart';
import 'package:todo_app/constant/app_constant.dart';
import 'package:todo_app/di/abstract_shared_app_injector.dart';
import 'package:todo_app/domain/todo/repo/todo_abstract_repository.dart';

import 'app_injector.config.dart';

final getIt = GetIt.instance;

@InjectableInit(usesNullSafety: true)
Future<void> configureDependencies() async => $initGetIt(getIt);

class AppInjector extends AbstractSharedAppInjector {

  @override
  AbstractTodoRepository get abstractTodoRepository =>
      getIt.get(instanceName: AppConstants.dIAstractTodoRepository);
}
