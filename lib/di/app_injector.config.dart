// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// InjectableConfigGenerator
// **************************************************************************

import 'package:get_it/get_it.dart' as _i1;
import 'package:injectable/injectable.dart' as _i2;
import 'package:sqflite/sqflite.dart' as _i5;

import '../domain/todo/repo/todo_abstract_repository.dart' as _i6;
import 'modules/abstract_todo_module.dart' as _i3;
import 'modules/database_module.dart' as _i7;
import 'modules/todo_module.dart'
    as _i4; // ignore_for_file: unnecessary_lambdas

// ignore_for_file: lines_longer_than_80_chars
/// initializes the registration of provided dependencies inside of [GetIt]
Future<_i1.GetIt> $initGetIt(_i1.GetIt get,
    {String? environment, _i2.EnvironmentFilter? environmentFilter}) async {
  final gh = _i2.GetItHelper(get, environment, environmentFilter);
  final databaseModule = _$DatabaseModule();
  final abstractTodoModule = _$AbstractTodoModule();
  gh.factory<_i3.AbstractTodoModule>(() => _i4.TodoModule());
  await gh.factoryAsync<_i5.Database>(() => databaseModule.database,
      preResolve: true);
  gh.singleton<_i6.AbstractTodoRepository>(
      abstractTodoModule.todoRepository(get<_i5.Database>()),
      instanceName: 'AbstractTodoRepository');
  return get;
}

class _$DatabaseModule extends _i7.DatabaseModule {}

class _$AbstractTodoModule extends _i3.AbstractTodoModule {}
