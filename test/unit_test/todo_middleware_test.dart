import 'dart:async';

import 'package:built_collection/built_collection.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/annotations.dart';
import 'package:todo_app/app/status.dart';
import 'package:todo_app/constant/app_constant.dart';
import 'package:todo_app/domain/todo/redux/middleware_actions.dart';
import 'package:mockito/mockito.dart';
import 'package:redux/redux.dart';
import 'package:redux_compact/redux_compact.dart';
import 'package:todo_app/app/app_state.dart';
import 'package:todo_app/domain/todo/entities/todo_entity.dart';
import 'package:todo_app/domain/todo/redux/compact_actions.dart';
import 'package:todo_app/domain/todo/redux/todo_state.dart';
import 'package:todo_app/domain/todo/repo/todo_abstract_repository.dart';

import 'todo_helpers.dart';

import 'todo_middleware_test.mocks.dart';


@GenerateMocks([AbstractTodoRepository])
void main() {
  // Setup
  final compactReducer = ReduxCompact.createReducer<AppState>();
  final compactMiddleware = ReduxCompact.createMiddleware<AppState>();

  late TodoState initialState;
  late Store<AppState> store;
  late MockAbstractTodoRepository mockRepo;

  setUp(() {
    initialState = TodoState();
    store = Store<AppState>(
      compactReducer,
      initialState: AppState(),
      middleware: [compactMiddleware],
    );
    mockRepo = MockAbstractTodoRepository();
    TodoCompactAction.repository = mockRepo;
  });

  group('Compact action middleware test', () {
    group('Test GetTodosCompactAction', () {
      Future runTest({
        dynamic action,
        required Future<List<TodoEntity>> Function(Invocation) mockAnswer,
        TodoState? state,
        dynamic matcher,
      }) async {
        if (state != null) {
          store = Store<AppState>(
            compactReducer,
            initialState: AppState(
              (builder) => builder..todoState = state.toBuilder(),
            ),
            middleware: [compactMiddleware],
          );
        }
        when(mockRepo.getTodos()).thenAnswer(mockAnswer);

        scheduleMicrotask(() {
          store.dispatch(action);
        });

        expect(
          store.onChange,
          emitsInOrder(
            matcher,
          ),
        );
      }

      Future runErrorTest(
          {dynamic action,
          required Error error,
          TodoState? state,
          dynamic matcher}) async {
        if (state != null) {
          store = Store(
            compactReducer,
            initialState:
                AppState((updates) => updates..todoState = state.toBuilder()),
            middleware: [compactMiddleware],
          );
        }

        when(mockRepo.getTodos()).thenThrow(error);

        scheduleMicrotask(() {
          store.dispatch(action);
        });

        expect(store.onChange, emitsInOrder(matcher));
      }

      test('success when get todos', () async {
        final todo1 = dumpTodo(
          id: '1',
        );
        final todo2 = dumpTodo(
          id: '1',
        );

        final rs = Future.value([todo1, todo2]);
        final result = await rs;
        final action =
            GetTodosMiddlewareTodoAction.create(statusKey: AppConstants.getKey);

        final loadingState = store.state.rebuild((updates) => updates
          ..todoState = initialState
              .rebuild(
                (builder) =>
                    builder..statuses[action.statusKey] = Status.loading(),
              )
              .toBuilder());

        final successState = store.state.rebuild((updates) => updates
          ..todoState = initialState
              .rebuild((builder) => builder
                ..statuses[action.statusKey] = Status.success()
                ..todos = ListBuilder<TodoEntity>(result))
              .toBuilder());

        await runTest(
          action: action,
          mockAnswer: (_) async {
            return rs;
          },
          state: initialState,
          matcher: <dynamic>[
            loadingState,
            successState,
          ],
        );
      });

      test('success when get todos refresh', () async {
        final todo1 = dumpTodo(
          id: '1',
        );
        final todo2 = dumpTodo(
          id: '1',
        );

        final rs = Future.value([todo1, todo2]);
        final result = await rs;
        final action = GetTodosMiddlewareTodoAction.create(
          statusKey: AppConstants.getKey,
          isRefresh: true,
        );

        final processState = store.state.rebuild((updates) => updates
          ..todoState = initialState
              .rebuild(
                (builder) =>
                    builder..statuses[action.statusKey] = Status.processing(),
              )
              .toBuilder());

        final successState = store.state.rebuild((updates) => updates
          ..todoState = initialState
              .rebuild((builder) => builder
                ..statuses[action.statusKey] = Status.success()
                ..todos = ListBuilder<TodoEntity>(result))
              .toBuilder());

        await runTest(
          action: action,
          mockAnswer: (_) async {
            return rs;
          },
          state: initialState,
          matcher: <dynamic>[
            processState,
            successState,
          ],
        );
      });

      test('error when get todos', () async {
        final action = GetTodosMiddlewareTodoAction.create(
          statusKey: AppConstants.getKey,
        );

        final processState = store.state.rebuild((updates) => updates
          ..todoState = initialState
              .rebuild(
                (builder) =>
                    builder..statuses[action.statusKey] = Status.loading(),
              )
              .toBuilder());

        final errorState = store.state.rebuild((updates) => updates
          ..todoState = initialState
              .rebuild(
                (builder) => builder
                  ..statuses[action.statusKey] = Status.error(
                    UnimplementedError().toString(),
                  ),
              )
              .toBuilder());

        await runErrorTest(
          action: action,
          error: UnimplementedError(),
          state: initialState,
          matcher: <dynamic>[
            processState,
            errorState,
          ],
        );
      });
    });
  });
}
