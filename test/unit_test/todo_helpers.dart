import 'package:todo_app/constant/app_constant.dart';
import 'package:todo_app/domain/todo/entities/todo_entity.dart';

TodoEntity dumpTodo({
  String id = '1',
  String title = "Todo1",
  String description = "abs",
  int isCompleted = AppConstants.incomplete,
}) {
  return TodoEntity(
    (updates) => updates
      ..id = id
      ..title = title
      ..description = description
      ..isCompleted = isCompleted,
  );
}
